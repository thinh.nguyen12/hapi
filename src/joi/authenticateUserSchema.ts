import Joi from 'joi';

export const UserRegisterSchema = Joi.object({
  username: Joi.string().alphanum().min(2).max(30).required(),
  password: Joi.string().required(),
  email: Joi.string().email(),
});

export const UserLoginSchema = Joi.object({
  username: Joi.string().alphanum().min(2).max(30).required(),
  password: Joi.string().required(),
});
