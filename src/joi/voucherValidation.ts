import Joi from "Joi";

export const getVouchersSchema = Joi.object({
    event_id: Joi.string().required().description('the id of the event')
});

export const createVouchersSchema = Joi.object({
    userEmail: Joi.string().email().required().description('the email of user sent voucher code')
});

