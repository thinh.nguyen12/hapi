import Joi from "Joi";

export const idEventSchema = Joi.object({
    event_id: Joi.string().required().description('the id of the event')
});

export const headerEventSchema = Joi.object({
    jwt_token: Joi.string().required()
}).options({ allowUnknown: true });

export const eventsSchema = Joi.object({
    name: Joi.string(),
    startTime: Joi.date().required(),
    endTime: Joi.date().required(),
    maxQuantityVoucher: Joi.number().required()
});

