import { Request, ResponseToolkit } from '@hapi/hapi';
import { startSession } from 'mongoose';
import { RequestVoucherInterface } from '../interface/Request';
import Event from '../models/Event';
import Voucher from '../models/Voucher';
import { addEmailToQueue } from '../utils/emailQueue';
import { handleCatchError } from '../utils/handleCatchError';
import { commitWithRetry } from '../utils/transaction';

const getRandomString = (length: Number): string => {
  let randomChars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  let result = '';
  for (let i = 0; i < length; i++) {
    result += randomChars.charAt(Math.floor(Math.random() * randomChars.length));
  }
  return result;
};

export const getVoucherOfEvent = async (req: Request, res: ResponseToolkit) => {
  try {
    const vouchers = await Voucher.find({ event: req.params.event_id });
    return res.response(vouchers);
  } catch (error) {
    res.response(handleCatchError(error)).code(500);
  }
};

export const createVoucher = async (req: Request, res: ResponseToolkit) => {
  const session = await startSession();
  session.startTransaction();
  try {
    const request = <RequestVoucherInterface>req;
    const event = await Event.findById(req.params.event_id);
    if (event) {
      if (event.voucherRemaining > 0) {
        const voucherCode = getRandomString(5);
        const newVoucher = new Voucher({ code: voucherCode, event: event._id });
        const voucherSaved = await newVoucher.save();
        const eventUpdated = await Event.findOneAndUpdate(
          { $and: [{ id: event._id }, { voucherRemaining: { $gt: 0 } }] },
          { $inc: { voucherRemaining: -1 } },
          { session, new: true },
        ); // voucher >0
        if (voucherSaved && eventUpdated) {
          await commitWithRetry(session);
          const addQueue = await addEmailToQueue({ userEmail: request.payload.userEmail, voucher: voucherSaved });
          return res.response({
            code: voucherSaved.code,
            event_id: voucherSaved.event,
            id: voucherSaved._id,
          });
        }
      } else {
        session.endSession();
        console.log('Vouchers are sold out');
        return res.response('Vouchers are sold out ').code(456);
      }
    }
    session.endSession();
    return res.response('No event was found').code(456);
  } catch (error) {
    await session.abortTransaction();
    session.endSession();
    res.response(handleCatchError(error)).code(500);
  }
};
