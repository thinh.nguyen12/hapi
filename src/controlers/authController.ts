import { Request, ResponseObject, ResponseToolkit } from '@hapi/hapi';
import * as bcrypt from 'bcrypt';
import { UserRequestInterface } from '../interface/Request';
import User from '../models/User';
import { handleCatchError } from '../utils/handleCatchError';
import { createToken } from '../utils/token';

export const register = async (req: Request, res: ResponseToolkit) => {
  try {
    const request = <UserRequestInterface>req;
    let user = new User();
    user.email = request.payload.email ? request.payload.email : '';
    user.username = request.payload.username;
    user.password = request.payload.password;
    user.admin = false;
    const salt = await bcrypt.genSalt(10);
    user.password = await bcrypt.hash(user.password, salt);
    const userSaved = await user.save();
    if (userSaved) {
      const { username, email = '', password } = userSaved;
      return res.response({ jwt_token: createToken(user), username, email, password });
    }
    return res.response('Create user failed');
  } catch (error) {
    res.response(handleCatchError(error)).code(500);
  }
};
