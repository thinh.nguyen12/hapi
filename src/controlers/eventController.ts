import { Request, ResponseObject, ResponseToolkit } from '@hapi/hapi';
import Boom from 'boom';
import * as jwt from 'jsonwebtoken';
import { startSession } from 'mongoose';
import { DecodeJwtInterface } from '../interface/jwt';
import { EventRequestInterface } from '../interface/Request';
import Event from '../models/Event';
import TrackingEvent from '../models/TrackingEvent';
import { commitWithRetry } from '../utils/transaction';
import { handleCatchError } from '../utils/handleCatchError';

export const createEvent = async (req: Request, res: ResponseToolkit) => {
  try {
    const payload = <EventRequestInterface>req.payload;
    const event = new Event(payload);
    const eventSaved = await event.save();
    return res.response(eventSaved);
  } catch (error) {
    res.response(handleCatchError(error)).code(500);
  }
};

export const getEvents = async (req: Request, res: ResponseToolkit) => {
  try {
    const events = await Event.find();
    return res.response(events);
  } catch (error) {
    res.response(handleCatchError(error)).code(500);
  }
};

export const getEvent = async (req: Request, res: ResponseToolkit) => {
  try {
    const eventfound = await Event.findById(req.params.event_id);
    if (eventfound) {
      return res.response(eventfound);
    }
    return res.response().code(404);
  } catch (error) {
    res.response(handleCatchError(error)).code(500);
  }
};

export const updateEvent = async (req: Request, res: ResponseToolkit) => {
  try {
    const payload = <EventRequestInterface>req.payload;
    // const updatedEvent = await Event.findByIdAndUpdate(req.params.id, { $set: { eventname: payload.eventname, password: payload.password } }, { new: true });
    const updatedEvent = await Event.findByIdAndUpdate(req.params.event_id, payload, { new: true });
    if (updatedEvent) {
      return res.response(updatedEvent);
    }
    return res.response().code(404);
  } catch (error) {
    res.response(handleCatchError(error)).code(500);
  }
};

export const deleteEvent = async (req: Request, res: ResponseToolkit) => {
  try {
    const deleteddEvent = await Event.findByIdAndDelete(req.params.event_id, {
      new: true,
    });
    if (deleteddEvent) {
      return res.response(deleteddEvent);
    }
    return res.response().code(404);
  } catch (error) {
    res.response(handleCatchError(error)).code(500);
  }
};

export const eventEditable = async (req: Request, res: ResponseToolkit) => {
  const session = await startSession();
  try {
    const request = <EventRequestInterface>req;
    const eventId = req.params.event_id;
    const decode = <DecodeJwtInterface>jwt.verify(request.headers.jwt_token, 'perfect');
    session.startTransaction();
    if (decode && decode.id) {
      const trackingEvent = await TrackingEvent.findOne({
        eventId,
      }).session(session);
      if (trackingEvent) {
        if (trackingEvent.editable) {
          const userEditing = await TrackingEvent.findOneAndUpdate(
            { eventId, editable: true },
            { userEditing: decode.id, editable: false },
            { session },
          );
          if (userEditing) return res.response('Allowed').code(200);
          else throw Boom.badRequest('Create tracking failed');
        } else {
          if (decode.id == trackingEvent.userEditing) {
            return res.response('Allowed').code(200);
          }
          return res.response('Not Allowed').code(409);
        }
      } else {
        const event = await Event.findOne({ _id: eventId });
        if (event) {
          const newTrackingEvent = new TrackingEvent();
          newTrackingEvent.eventId = eventId;
          newTrackingEvent.editable = false;
          newTrackingEvent.userEditing = decode.id;
          await newTrackingEvent.save();
        } else {
          throw Boom.notFound('No event was found');
        }
      }
      const count = await TrackingEvent.countDocuments({
        eventId,
      }).session(session);
      if (count > 1) throw Boom.badRequest();
      await commitWithRetry(session);
      session.endSession();
      return res.response('Allowed').code(200);
    } else {
      throw Boom.badRequest('Token invalid');
    }
  } catch (error) {
    await session.abortTransaction();
    session.endSession();
    res.response(handleCatchError(error)).code(500);
  }
};

// export const eventEditable = async (req: Request, res: ResponseToolkit) => {
//     try {
//         const request = <EventRequestInterface>(req);
//         const eventId = req.params.event_id;
//         const decode = <DecodeJwtInterface>jwt.verify(request.headers.jwt_token, 'perfect');
//         if (decode && decode.id) {
//             const trackingEvent = await TrackingEvent.findOneAndUpdate({ eventId }, { $inc: { count: 1 } });
//             if (trackingEvent) {
//                 if (trackingEvent.editable) {
//                     const userEditing = await TrackingEvent.findOneAndUpdate({ eventId, editable: true }, { userEditing: decode.id, editable: false });
//                     if (userEditing) return res.response('Allowed').code(200);
//                     else throw Boom.badRequest('Create tracking failed');
//                 } else {
//                     return res.response('Not Allowed').code(409);
//                 }
//             } else {
//                 const event = await Event.findOne({ _id: eventId });
//                 if (event) {
//                     const newTrackingEvent = new TrackingEvent();
//                     newTrackingEvent.eventId = eventId;
//                     newTrackingEvent.editable = false;
//                     newTrackingEvent.userEditing = decode.id;
//                     const trackingEventSaved = await newTrackingEvent.save();
//                     if (trackingEventSaved) return res.response('Allowed').code(200);
//                     else throw Boom.badRequest('Create tracking failed');
//                 } else {
//                     throw Boom.notFound('No event was found')
//                 }
//             }
//         } else {
//             throw Boom.badRequest('Token invalid');
//         }
//     } catch (error) {
//         if (typeof error === "string") {
//             return res.response(error.toUpperCase()).code(500);
//         } else if (error instanceof Error) {
//             return res.response(error.message).code(500);
//         }
//         return res.response('error').code(500); //TODO
//     }
// }

export const eventReleaseEdit = async (req: Request, res: ResponseToolkit) => {
  const session = await startSession();
  try {
    const eventId = req.params.event_id;
    session.startTransaction();
    const trackingEvent = await TrackingEvent.findOneAndUpdate({ eventId }, { $inc: { count: 1 } }, { session });
    if (trackingEvent) {
      const release = await TrackingEvent.findOneAndUpdate(
        { eventId },
        { userEditing: null, editable: true },
        { session },
      );
      await commitWithRetry(session);
      session.endSession();
      if (release) return res.response('Released').code(200);
      return res.response('Release failed').code(409);
    } else {
      const event = await Event.findOne({ _id: eventId }).session(session);
      await commitWithRetry(session);
      session.endSession();
      if (event) {
        return res.response('Released').code(200);
      } else {
        throw Boom.notFound('No event was found');
      }
    }
  } catch (error) {
    await session.abortTransaction();
    session.endSession();
    res.response(handleCatchError(error)).code(500);
  }
};

export const eventMaintainEdit = async (req: Request, res: ResponseToolkit) => {
  const session = await startSession();
  try {
    const request = <EventRequestInterface>req;
    const eventId = req.params.event_id;
    const decode = <DecodeJwtInterface>jwt.verify(request.headers.jwt_token, 'perfect');
    session.startTransaction();
    if (decode && decode.id) {
      const trackingEvent = await TrackingEvent.findOneAndUpdate(
        { eventId, editable: false, userEditing: decode.id },
        { updateAt: new Date() },
        { session },
      );
      await commitWithRetry(session);
      session.endSession();
      if (trackingEvent) return res.response('Maintain editing successfully').code(200);
      else return res.response('Maintain editing failed').code(409);
    } else {
      throw Boom.badRequest('Token invalid');
    }
  } catch (error) {
    await session.abortTransaction();
    session.endSession();
    res.response(handleCatchError(error)).code(500);
  }
};
