import { Request, ResponseToolkit } from '@hapi/hapi';
import Queue from 'bull';
import { EditableRequestInterface } from '../interface/Request';
import User from '../models/User';
import { handleCatchError } from '../utils/handleCatchError';

const createQueue = new Queue('create user', 'redis://127.0.0.1:6379');
createQueue.process(async (job, done) => {
  job.progress(42);
  console.log(job.data);
  done();
});
export const createUser = async (req: Request, res: ResponseToolkit) => {
  try {
    const options = {
      delay: 20000,
      attempts: 2,
    };
    createQueue.add(req.payload, options);
    const user = new User(req.payload);
    const userSaved = await user.save();
    return res.response(userSaved);
  } catch (error) {
    res.response(handleCatchError(error)).code(500);
  }
};

export const getUsers = async (req: Request, res: ResponseToolkit) => {
  try {
    const users = await User.find();
    return res.response(users);
  } catch (error) {
    res.response(handleCatchError(error)).code(500);
  }
};

export const getUser = async (req: Request, res: ResponseToolkit) => {
  try {
    const userfound = await User.findById(req.params.id);
    if (userfound) {
      return res.response(userfound);
    }
    return res.response().code(404);
  } catch (error) {
    res.response(handleCatchError(error)).code(500);
  }
};

export const updateUser = async (req: Request, res: ResponseToolkit) => {
  try {
    // const payload = new User(req.payload);
    const payload: Request = <EditableRequestInterface>req.payload;
    // const updatedUser = await User.findByIdAndUpdate(req.params.id, { $set: { username: payload.username, password: payload.password } }, { new: true });
    const updatedUser = await User.findByIdAndUpdate(req.params.id, payload, { new: true });
    if (updatedUser) {
      return res.response(updatedUser);
    }
    return res.response().code(404);
  } catch (error) {
    res.response(handleCatchError(error)).code(500);
  }
};

export const deleteUser = async (req: Request, res: ResponseToolkit) => {
  try {
    const deleteddUser = await User.findByIdAndDelete(req.params.id, {
      new: true,
    });
    if (deleteddUser) {
      return res.response(deleteddUser);
    }
    return res.response().code(404);
  } catch (error) {
    res.response(handleCatchError(error)).code(500);
  }
};
