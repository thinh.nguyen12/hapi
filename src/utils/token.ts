import * as jwt from 'jsonwebtoken';
import { IUser } from '../models/User';

export const createToken = (user: IUser) => {
    let scopes: string = '';
    if (user.admin) {
        scopes = 'admin';
    }
    // Sign the JWT
    return jwt.sign({ id: user._id, username: user.username, scope: scopes }, 'perfect', { algorithm: 'HS256', expiresIn: "24h" });
}
