import Queue from "bull";
import { emailQueueInterface } from "../interface/emailQueueInterface";
import { mailer } from "../utils/mailer";

const emailQueue = new Queue('send email', 'redis://127.0.0.1:6379');

export const addEmailToQueue = async (data: emailQueueInterface) => {
    try {
        return await emailQueue.add(data, { removeOnComplete: true });
    } catch (error) {
        console.log(error);
    }
}
emailQueue.on("error", function(err){ console.log("error", err) });
emailQueue.on("failed", function(job,err){ console.log("failed", job, err)});
emailQueue.process(async (job, done) => {
    try {
        const { userEmail = 'ntthanhien@gmail.com', voucher = '' } = job.data;
        await mailer(userEmail, 'Voucher', voucher.code);
        console.log('<h3>Your email has been sent successfully.</h3>')
    } catch (error) {
        console.log(error)
    }
    done();
});