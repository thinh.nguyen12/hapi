import { Agenda } from "agenda";
import mongoose from "mongoose";


// const agenda = new Agenda({
//     db: { address: connectionString, collection: 'User' },
//     processEvery: '1 minutes'
// });

// agenda.define('check database connection', function (job, done) {
//     console.log('Agenda successfully worked');
//     done();
// });
// (async function () {
//     const checkDb = agenda.create('check database connection', { username: 'test', password: 'test' });
//     await agenda.start();
//     await checkDb.repeatEvery('1 minutes').save();
// })();


const connectionString = 'mongodb+srv://thinhnguyenhdw:17031999a@voucher-application.qmt0z.mongodb.net/voucher-application?retryWrites=true&w=majority';


const agenda = new Agenda({ db: { address: connectionString } });


agenda.define("check database connection", async () => {
    try {
        await mongoose.connect(connectionString);
        console.log('Database connection is gooo')
    } catch (error) {
        console.log(error);
    }

});

(async function () {
    // IIFE to give access to async/await
    await agenda.start();
    await agenda.every("1 minutes", "check database connection");
})();