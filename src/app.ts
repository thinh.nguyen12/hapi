import * as Hapi from '@hapi/hapi';
import { Server } from '@hapi/hapi';
import Inert from '@hapi/inert';
import Vision from '@hapi/vision';
import * as HapiSwagger from 'hapi-swagger';
import { authRoutes } from './routes/authRoutes';
import { eventsRoutes } from './routes/eventRoutes';
import { userRoutes } from './routes/userRoutes';
import { voucherRoutes } from './routes/voucherRoutes';

export const init = async () => {
    const server: Server = new Server({
        port: 3000,
        host: 'localhost'
    });
    const swaggerOptions: HapiSwagger.RegisterOptions = {
        info: {
            title: 'API Documentation for coucher application'
        }
    };
    const plugins: Array<Hapi.ServerRegisterPluginObject<any>> = [
        {
            plugin: Inert
        },
        {
            plugin: Vision
        },
        {
            plugin: HapiSwagger,
            options: swaggerOptions
        }
    ];
    await server.register(plugins);
    await server.register(require('hapi-auth-jwt2'));
    server.auth.strategy('jwt', 'jwt',
        {
            key: process.env.SECRECT_KEY, // Never Share your secret key
            validate: () => { }, // validate function defined above,
            verifyOptions: { algorithms: ['HS256'] }
        });
    userRoutes(server);
    eventsRoutes(server);
    voucherRoutes(server);
    authRoutes(server);
    await server.start();
    console.log(`Server is running on ${server.info.uri}`);
};

process.on("unhandledRejection", err => {
    console.log(err);
    process.exit(0);
});