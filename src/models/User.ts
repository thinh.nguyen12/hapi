import { Schema, Document, model } from "mongoose";
import bcrypt from "bcrypt";

export interface IUser extends Document {
    email: string,
    username: string,
    password: string,
    admin: boolean
    // comparePassword: (password: string) => Promise<Boolean>,
}

const userSchema = new Schema({
    email: {
        type: String,
        default: ''
    },
    username: {
        type: String,
        required: true,
        index: { unique: true },
        lowercase: true
    },
    password: {
        type: String,
        required: true
    },
    admin: {
        type: Boolean,
        required: true
    }
}, { timestamps: true });

// userSchema.pre<IUser>('save', async function (next) {
//     const user = this;
//     if (!user.isModified('password')) return next();

//     const salt = await bcrypt.genSalt(10);
//     user.password = await bcrypt.hash(user.password, salt);
//     next();
// })



// userSchema.methods.comparePassword = async function (password: string): Promise<Boolean> {
//     return await bcrypt.compare(password, this.password);
// };

export default model<IUser>("User", userSchema);