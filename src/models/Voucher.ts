import { Schema, Document, model } from "mongoose";

export interface IVoucher extends Document {
    code: string,
    event: string
}

const voucherSchema = new Schema({
    code: {
        type: String,
        required: true,
        unique: true
    },
    event: {
        type: Schema.Types.ObjectId,
        ref: 'Events',
        required: true
    }
});

export default model<IVoucher>("Voucher", voucherSchema);