import { Document, model, Schema } from "mongoose";
export interface ITrackingEvent extends Document {
    eventId: string,
    editable: boolean,
    userEditing: string,
    count: number
}

const trackingEventSchema = new Schema({
    eventId: {
        type: Schema.Types.ObjectId,
        ref: 'Event',
        required: true
    },
    editable: {
        type: Boolean,
        required: true
    },
    userEditing: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    },
    count: {
        type: Number,
        default: 0
    },
    updateAt: {
        type: Date,
        default: Date.now,
        expires: 300
    }
});

// trackingEventSchema.pre<ITrackingEvent>('save', async function (next) {
//     const event = this;
//     event.voucherRemaining = event.maxQuantityVoucher;
//     next();
// })
// trackingEventSchema.index({ createAt: 1 }, { expireAfterSeconds: 300 });
export default model<ITrackingEvent>("TrackingEvent", trackingEventSchema);