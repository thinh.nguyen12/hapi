import { Schema, Document, model } from "mongoose";
export interface IEvent extends Document {
    name: string,
    startTime: Date,
    endTime: Date,
    maxQuantityVoucher: number,
    voucherRemaining: number
}

const eventSchema = new Schema({
    name: {
        type: String,
    },
    startTime: {
        type: Date,
        required: true
    },
    endTime: {
        type: Date,
        required: true
    },
    maxQuantityVoucher: {
        type: Number,
        required: true
    },
    voucherRemaining: {
        type: Number
    }
});

eventSchema.pre<IEvent>('save', async function (next) {
    const event = this;
    event.voucherRemaining = event.maxQuantityVoucher;
    next();
})

export default model<IEvent>("Event", eventSchema);