import { Request, ResponseToolkit } from '@hapi/hapi';
import Boom from 'boom';
import { UserRequestInterface } from '../interface/Request';
import User from '../models/User';

export const verifyUniqueUser = async (request: Request, res: ResponseToolkit) => {
    const req = <UserRequestInterface>(request);
    const user = await User.findOne({ username: req.payload.username });
    if (user) {
        if (user.username == req.payload.username) {
            throw Boom.badRequest('Username taken');
        }
        if (user.email == req.payload.email) {
            throw Boom.badRequest('Email taken');
        }
    }
    return res.response(req.payload);
}