import { Request, ResponseToolkit } from '@hapi/hapi';
import User from '../models/User';
import { UserRequestInterface } from '../interface/Request';
import bcrypt from 'bcrypt';
import Boom from 'boom';

export const verifyCredentials = async (request: Request, res: ResponseToolkit) => {
    const req = <UserRequestInterface>(request);
    const user = await User.findOne({ username: req.payload.username });
    const password = req.payload.password;
    if (user) {
        const compare = await bcrypt.compare(password, user.password);
        if (compare) {
            return res.response(user);
        } else {
            throw Boom.badRequest('Username or password is incorrect!');
        }
    } else {
        throw Boom.badRequest('Username or password is incorrect!');
    }
}

