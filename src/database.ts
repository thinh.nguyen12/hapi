require('dotenv').config();
import mongoose from "mongoose";
import { Agenda } from "agenda";

const connectionString = `mongodb+srv://${process.env.DB_USERNAME}:${process.env.DB_PASSWORD}@voucher-application.qmt0z.mongodb.net/voucher-application?retryWrites=true&w=majority`;

// mongoose.connect('mongodbL//localhost/testdb')
//     .then(db => console.log('Database is connected'))
//     .catch(err => console.log(err));
const connectDB = async () => {
    try {
        await mongoose.connect(connectionString);
        console.log('MongoDB connected');
        const agenda = new Agenda({ db: { address: connectionString } });

        agenda.define("check database connection", async () => {
            await mongoose.connect(connectionString);
            console.log('Database connection is good');
        });
        await agenda.start();
        await agenda.every("1 minutes", "check database connection");
    } catch (error) {
        console.log(error);
        process.exit(1);
    }
}
connectDB();
