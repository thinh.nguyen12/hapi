import { IVoucher } from "../models/Voucher";

export interface emailQueueInterface {
    userEmail: string,
    voucher: IVoucher
}