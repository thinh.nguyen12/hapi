import * as Hapi from "@hapi/hapi";

export interface CredentialInterface extends Hapi.AuthCredentials {
    id: String;
}

export interface RequestAuthInterface extends Hapi.RequestAuth {
    credentials: CredentialInterface;
}

export interface RequestInterface extends Hapi.Request {
    auth: RequestAuthInterface;
}

export interface EditableRequestInterface extends RequestInterface {
    payload: {
        username: string;
        password: string;
    };
}

export interface RequestVoucherInterface extends RequestInterface {
    payload: {
        userEmail: string;
    };
}

export interface EventRequestInterface extends RequestInterface {
    payload: {
        name: string;
        startTime: Date;
        endTime: Date;
        maxQuantityVoucher: number;
    };
}

export interface UserRequestInterface extends RequestInterface {
    payload: {
        username: string;
        email: string;
        password: string;
    };
}
