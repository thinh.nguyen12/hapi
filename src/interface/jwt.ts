export interface DecodeJwtInterface {
    id: string,
    username: string,
    scope: string,
    iat: number,
    exp: number
}