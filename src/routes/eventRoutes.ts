import { Server } from '@hapi/hapi';
import {
  createEvent,
  deleteEvent,
  eventEditable,
  eventMaintainEdit,
  eventReleaseEdit,
  getEvent,
  getEvents,
  updateEvent,
} from '../controlers/eventController';
import { eventsSchema, headerEventSchema, idEventSchema } from '../joi/eventValidation';

export const eventsRoutes = (server: Server) => {
  server.route({
    method: 'GET',
    path: '/events',
    options: {
      handler: getEvents,
      description: 'Get all events',
      notes: 'Returns all events',
      tags: ['api'],
    },
  });

  server.route({
    method: 'GET',
    path: '/events/{event_id}',
    options: {
      handler: getEvent,
      description: 'Get event by id',
      notes: 'Returns an event by the event id passed in the path',
      tags: ['api'], // ADD THIS TAG
      validate: {
        params: idEventSchema,
      },
    },
  });

  server.route({
    method: 'POST',
    path: '/events',
    options: {
      handler: createEvent,
      description: 'Create an event',
      notes: 'Create an event by the information of the event passed in the payload',
      tags: ['api'], // ADD THIS TAG
      validate: {
        payload: eventsSchema,
      },
    },
  });

  server.route({
    method: 'PUT',
    path: '/events/{event_id}',
    options: {
      handler: updateEvent,
      description: 'Update an event',
      notes: 'Update an event by the iformation of the event passed in the payload',
      tags: ['api'], // ADD THIS TAG
      validate: {
        payload: eventsSchema,
      },
    },
  });

  server.route({
    method: 'DELETE',
    path: '/events/{event_id}',
    options: {
      handler: deleteEvent,
      description: 'Delete an event by id',
      notes: 'Delete an event by the event id passed in the path',
      tags: ['api'], // ADD THIS TAG
      validate: {
        params: idEventSchema,
      },
    },
  });

  server.route({
    method: 'POST',
    path: '/events/{event_id}/editable/me',
    options: {
      handler: eventEditable,
      description: 'Check an event is still editable and mark current editing user',
      notes: 'Check by event id in path and mark current editing user by id',
      tags: ['api'], // ADD THIS TAG
      validate: {
        params: idEventSchema,
        headers: headerEventSchema,
      },
    },
  });

  server.route({
    method: 'POST',
    path: '/events/{event_id}/editable/release',
    options: {
      handler: eventReleaseEdit,
      description: 'Allows the frontend to release edits of the current user',
      notes: 'Alows by event id in path',
      tags: ['api'], // ADD THIS TAG
      validate: {
        params: idEventSchema,
      },
    },
  });

  server.route({
    method: 'POST',
    path: '/events/{event_id}/editable/maintain',
    options: {
      handler: eventMaintainEdit,
      description: 'Maintain time editing of user',
      notes: 'Maintain by event id in path and token of user',
      tags: ['api'], // ADD THIS TAG
      validate: {
        params: idEventSchema,
        headers: headerEventSchema,
      },
    },
  });
};
