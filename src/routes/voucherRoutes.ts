import { Server } from '@hapi/hapi';
import { createVoucher, getVoucherOfEvent } from "../controlers/voucherController";
import { createVouchersSchema, getVouchersSchema } from '../joi/voucherValidation';

export const voucherRoutes = (server: Server) => {
    server.route({
        method: 'GET',
        path: '/vouchers/{event_id}',
        options: {
            handler: getVoucherOfEvent,
            description: 'Get all vouchers of an event',
            notes: 'Returns all vouchers of an event by the event id passed in the path',
            tags: ['api'], // ADD THIS TAG
            validate: {
                params: getVouchersSchema
            }
        }
    });
    server.route({
        method: 'POST',
        path: '/vouchers/{event_id}',
        options: {
            handler: createVoucher,
            description: 'Generate new voucher for user',
            notes: 'Generate new voucher of an event by the event id passed in the path',
            tags: ['api'], // ADD THIS TAG
            validate: {
                params: getVouchersSchema,
                payload: createVouchersSchema
            }
        }
    });
}

