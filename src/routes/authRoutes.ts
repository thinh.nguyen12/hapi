import { Server } from '@hapi/hapi';
import { verifyCredentials } from '../auth/userAuth';
import { register } from '../controlers/authController';
import { UserRegisterSchema, UserLoginSchema } from '../joi/authenticateUserSchema';
import { createToken } from '../utils/token';
import { verifyUniqueUser } from '../auth/userValidation';

export const authRoutes = (server: Server) => {
  server.route({
    method: 'POST',
    path: '/login',
    handler: (req, res) => {
      if (req.pre.user) return res.response({ jwt_token: createToken(req.pre.user) }).code(201);
      else return res.response('Username or password incorrect');
    },
    options: {
      pre: [{ method: verifyCredentials, assign: 'user' }],
      validate: {
        payload: UserLoginSchema,
      },
      description: 'Login',
      notes: 'Success or not',
      tags: ['api'], // ADD THIS TAG
    },
  });

  server.route({
    method: 'POST',
    path: '/register',
    handler: register,
    options: {
      pre: [{ method: verifyUniqueUser }],
      validate: {
        payload: UserRegisterSchema,
      },
      description: 'Register account',
      notes: 'Returns token for user',
      tags: ['api'], // ADD THIS TAG
    },
  });
};
