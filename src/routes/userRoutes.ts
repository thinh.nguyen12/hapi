import { Server } from '@hapi/hapi';
import { createUser, getUser, getUsers, deleteUser, updateUser } from "../controlers/userController";
import Joi from "Joi";

const userSchema = Joi.object({
    username: Joi.string()
        .alphanum()
        .min(3)
        .max(30)
        .required(),
    password: Joi.string()
        .pattern(new RegExp('^[a-zA-Z0-9]{3,30}$')),
});

export const userRoutes = (server: Server) => {
    server.route({
        method: 'POST',
        path: '/user',
        handler: createUser,
        // options: {
        //     validate: {
        //         payload: userSchema
        //     }
        // }

    });

    server.route({
        method: 'GET',
        path: '/user/{id}',
        handler: getUser,
        options: {
            validate: {
                params: Joi.required()
            }
        }
    });

    server.route({
        method: 'GET',
        path: '/users',
        handler: getUsers
    });

    server.route({
        method: 'PUT',
        path: '/user/{id}',
        handler: updateUser,
        options: {
            validate: {
                params: Joi.required(),
                payload: userSchema
            }
        }

    });

    server.route({
        method: 'DELETE',
        path: '/user/{id}',
        handler: deleteUser
    });
}

